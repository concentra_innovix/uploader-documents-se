﻿<?php
set_time_limit(120);
ini_set('allow_url_fopen', 'on');

require_once 'lib/nusoap.php';
// El fichero test.xml contiene un documento XML con un elemento ra�z y, al
// menos, un elemento /[raiz]/titulo.

class Monitor {
    public $monitor;
    public function Loadxml($FullPath)
    {
       
        $xml = simplexml_load_file($FullPath);
        /*if (file_exists("C:\ScannedDocuments\xml\UAPA2018.xml")) 
        {
           //$xml = simplexml_load_file($FullPath);
           //exit;
           print_r('Entró'); 

           //return $xml;
        } 
        else 
        {
            //return 'Error';
            print_r('No entró');
        }
        */
        return $xml;
        //exit;
    }


    public function Xml($xml, $filename)
    {
        $total_documentos = count($xml->Document);
        $totalDocumentos = count($xml->Document);   
        $total_indices = count($xml->Document->IndexValue);
        $result = '';
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        $atribute ='';
        $documentos = $array['Document'];

        $title = csv();
        $oficio = $documentos[0]['IndexValues']['IndexValue'][6]['Value'];
        $categoria = $documentos[0]['IndexValues']['IndexValue'][1]['Value'];
       
        //valida si existe el oficio
        $result = oficioExist($oficio);
         //print_r($result);
        //exit;
        print("Validando Oficio...\n\n");
   
        
        if(oficioExist($oficio) > 0)
        {
            print("Estatus: Oficio iniciado anteriormente...\n");
            //Comprueba si existe el expediente
            foreach($documentos as $documento=>$actual)
            {
                $indices = $actual['IndexValues']['IndexValue'];
                $expediente = $indices[4]['Value']; 
                print("Validando expediente...\n");
                //print_r($expediente."\n");
  
                if(expedienteExist($expediente) >0)
                {
                    //foreach document
                    print_r("Validando documento...\n");
            
                    $categoria = $actual['IndexValues']['IndexValue'][1]['Value'];
                    print_r('Subiendo documento '.$title[$indices[1]['Value']]." a Softexpert\n");
                    prepareUpload($actual,$title);
                  
    
                }
                else
                {
                   
                    print_r("Se sube cada documento del expediente completo sin validar cada documento\n");
                    continue;
                }
                print_r("\n");
            }

           
        }
        else
        {
            print("Estatus: Oficio nuevo...\n");
            print_r("Se sube el oficio completo sin hacer ninguna comparacion\n");
            foreach($documentos as $came=>$ha)
            {
                prepareUpload($ha,$title);
            }
        }
           
    
        rename("C:\\ScannedDocuments\\xml\\".$filename, "C:\\ScannedDocuments\\Procesado\\".$filename);
    
        echo("OFICIO CONCLUIDO");
        echo("=================================================================");
    }
}
$directorio = "C:\ScannedDocuments\xml";

$archivos  = scandir($directorio);

$monitor = new Monitor();
$filename = $archivos[2];
$extension = explode(".", $filename);
$extension = end($extension);
$fullpath = "C:\\ScannedDocuments\\xml\\".$filename;
$response;
if($extension == 'xml')
{
    $response = $monitor ->Loadxml($fullpath);
    if($response != 'Error')
    {
        $monitor->Xml($response,$filename);
    }
}
else 
{
    unlink($fullpath);
    print_r('El archivo colocado en el directorio no cumplia con el formato esperado, ha sido borrado');
}

function validate($body, $method)
{
    $url = "http://se.servicios.gob.do:83/api/validate/".$method;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Bearer DdkcXlwEWpycqiXYPtvaC6ALIP34hjKmTlVU6iRqJDrF0ip7LbzuapJO8dYs'
    ));
    $response = curl_exec($ch);
    $result = json_decode($response, true);
    return $result;
}


function oficioExist($oficio)
{
   $body = [
       "oficio" => $oficio
   ];

   return validate($body,'oficio');

}

function expedienteExist($expediente)
{
    $body = [
        "expediente" => $expediente
    ];

    return validate($body,'expediente');
}

function documentExist($oficio,$expediente,$categoria)
{
    $body = [
        "oficio" => $oficio,
        "expediente" => $expediente,
        "categoria" => $categoria
    ];

    return validate($body,'documento');
}



function newDocument($data, $atrib, $title)
{
    $dc_ws = new nusoap_client('https://se.servicios.gob.do/softexpert/webserviceproxy/se/ws/dc_ws.php',false);
    $dc_ws->setCredentials('splws05','splws05');
    $params = array('idcategory' => $data[1]['Value'],'IDDOCUMENT'=>'','title' =>utf8_decode($title),'DSRESUME'=>'','DTDOCUMENT'=>'','ATTRIBUTES'=>$atrib);
    $request = $dc_ws ->call('newDocument',$params);
    $result = $request;
    $separado = explode(": ", $request);
   
    $DocumentID = $separado[1];
   
    return $DocumentID;
}



function UploadFile($doc,$ruta,$title){
    $client = new nusoap_client('https://se.servicios.gob.do/softexpert/webserviceproxy/se/ws/dc_ws.php',false);
    $client->setCredentials('splws01','splws01');
    $nombre = str_replace(' ','_',$title);
    $b64Doc = chunk_split(base64_encode(file_get_contents($ruta))); 

    $items = array('NMFILE'=>utf8_decode($nombre).'.pdf','BINFILE'=> $b64Doc, 'ERROR'=>'');
    $file = array('item'=>$items);
    $params = array('IDDOCUMENT' => $doc,'IDREVISION'=>'00','IDUSER' =>'40200532378','FILE'=>$file);
    $response = $client->call('uploadEletronicFile',$params);
    //print_r($client);
    return $response ;

}

 function prepareUpload($ha,$title)
{
        $number = $ha['Number'];
       // echo "El documento ".$number." contiene los siguientes Indices: ".PHP_EOL;
        $indices = $ha['IndexValues']['IndexValue'];
        
        $oficio = $indices[6]['Value'];
        $expediente = $indices[4]['Value'];
        //$categoria = $indices[];
               
        $atribute = "";
        for ($i=2; $i <count($indices) ; $i++) 
        { 
            $valor = $indices[$i]['Label']."=".$indices[$i]['Value'].";";
            $atribute =  $atribute.$valor;   
        }
        //print_r( $title[$indices[1]['Value']]);
        
        $idDoc =  newDocument($indices,$atribute, $title[$indices[1]['Value']]);
   
       /* while ($result == '') 
        {
            $result = newDocument($indices,$atribute,$title[$indices[1]['Value']]);
        }
        */

       $resultado = UploadFile($idDoc,$ha['FileName'],$title[$indices[1]['Value']]);
       
       print_r('Documento: '.$title[$indices[1]['Value']]."\n");
       print_r('ID: '.$idDoc."\n");
       print_r('Resultado: '.$resultado."\n");
  
       echo("");
       echo("".PHP_EOL);
       $atribute = '';
    
}

function csv(){
        
    $array;
    $registros = array();
    if (($fichero = fopen("C:\Monitor\Uploader\archivo.csv", "r")) !== FALSE) 
    {
        $nombres_campos = fgetcsv($fichero, 0, ",", "\"", "\"");
        $num_campos = count($nombres_campos);

        while (($datos = fgetcsv($fichero, 0, ",", "\"", "\"")) !== FALSE) 
        {
            $array[$datos[0]] = $datos[1];
                
        }
                   
        fclose($fichero);

    }
    return $array;

    }




    
?>